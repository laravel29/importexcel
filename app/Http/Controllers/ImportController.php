<?php

namespace App\Http\Controllers;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class ImportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //

    public function import(Request $request)
    {
        $file = $request->file('file');
        if($request->hasFile('file') && $file != null ){
            $fileEx = $file->getClientOriginalExtension();

            if ($fileEx != 'xlsx' && $fileEx != 'xls') {
                $res['success'] = false;
                $res['message'] = 'File yg diperbolehkan hanya .xls .xlsx, ini file '.$fileEx;
                return response($res);
            }else{
                $path = $file->getRealPath();
                
                $dataImport = Excel::load($path)->all();
                foreach ($dataImport as $key => $value) {
                    echo 'Mapel :'.$value->mapel.' Soal :'.$value->soal.', <br />';
                }
                die();

            }
        }
    }
}
